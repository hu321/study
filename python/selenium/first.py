from selenium import webdriver
from selenium.common.exceptions import NoSuchElementException
from time import ctime

driver = webdriver.Chrome()


# 设置隐式等待为10s
def baidu_test():
    driver.implicitly_wait(10)
    driver.init()
    driver.get("http://www.baidu.com")
    driver.add_cookie({"name": "urlfrom", "value": "121113803"})
    driver.find_element_by_id("kw").send_keys("selenium")

    print(driver.page_source)


def zhilian_test():
    driver.implicitly_wait(10)
    cookies = {
        'dywec':
        '95841923',
        'dywez':
        '95841923.1529465835.1.1.dywecsr=(direct)|dyweccn=(direct)|dywecmd=(none)|dywectr=undefined',
        '__utmc':
        '269921210',
        '__utmz':
        '269921210.1529465835.1.1.utmcsr=(direct)|utmccn=(direct)|utmcmd=(none)',
        'at':
        '9f100534755e4a9bba11b298e96f4aeb',
        'Token':
        '9f100534755e4a9bba11b298e96f4aeb',
        'rt':
        '93c010df4d5643c39a93ad29963d88af',
        'RDsUserInfo':
        '3d692e695671417152775175596a537545775d695a695f714c7129772775546a0c7515770a690f690271457155775975526a3775247755695b6950713571217754755d6a57754377516953695a7146715d775275286a30754d775e6947695971477145775875586a5e7543775f6951692a713b7158775975526a3175317755692069267140715d7759755e6a5175457758695e695b714c7130773d75546a557546775869516938713e7158775b75526a6',
        'uiioit':
        '3b622a6459640e6440644f6a586e556e516451385477507751682c622a6459640c644464446a516e5b6e556455385c775f776',
        'zp-route-meta':
        'uid=678075504,orgid=43399118',
        'login_point':
        '113072494',
        'NTKF_T2D_CLIENTID':
        'guest3F996E8C-C6E9-97B8-D45C-1B44DC6303F7',
        'nTalk_CACHE_DATA':
        '{uid:kf_9051_ISME9754_43399118,tid:1529465855075417}',
        'adShrink':
        '1',
        'sts_deviceid':
        '1641b44de1fa6-05888575e3e0ed-17376950-1296000-1641b44de2037c',
        'sts_sg':
        '1',
        'zp_src_url':
        'https%3A%2F%[图片]2Fpassport.zhaopin.com%2Forg%2Flogin',
        'rd_resume_actionId':
        '1530144205504',
        'rd_resume_srccode':
        '402101',
        'dywea':
        '95841923.2454339827292757500.1529465835.1529468678.1529478050.3',
        '__utma':
        '269921210.1536208372.1529465835.1529468678.1529478051.3',
        '__utmt':
        '1',
        'sts_evtseq':
        '1',
        'sts_sid':
        '1641bff102430a-0b9bc8b9e1a08f-17376950-1296000-1641bff102524b',
        'dyweb':
        '95841923.2.10.1529478050',
        '__utmb':
        '269921210.2.10.1529478051',
    }
    driver.get("https://zhaopin.com")
    driver.delete_all_cookies()

    #   `driver.get("https://rd5.zhaopin.com/search")
    for key, value in cookies.items():
        driver.add_cookie({
            "name": key,
            "value": value,
            "domain": '.zhaopin.com'
        })
    driver.implicitly_wait(3)
    driver.get("https://zhaopin.com")
    #driver.get("https://rd5.zhaopin.com/search")


if __name__ == '__main__':
    #baidu_test()
    zhilian_test()
