import numpy as np

from math import log, exp


def sigmoid(inX):
    return 1.0 / (1 + np.exp(-inX))


np.set_printoptions(precision=50)
print(sigmoid(-8))
