import redis

# 基本使用
print('''
基类 StrictRedis
子类 Redis
实现了Redis 的大部分命令

可以在创建实例的时候指定db参数，选择连接的数据库
''')
r = redis.Redis('qiukusha.cn')

print(r.get('name'))

print('''
可以看到输出是 byte类型
''')

# 连接池
'''
连接池的作用是免去每次执行命令的时候的连接和断开开销，而是维持多个不断的连接
'''
connection_pool = redis.ConnectionPool(host='qiukusha.cn')

r = redis.Redis(connection_pool=connection_pool)

print(r.get('name'))

# 管道命令
'''
管道是非阻塞执行，可以用来提升执行速度。未使用
管道的时候，一条命令执行完了才会执行下一条，然后使用管道之后会
一直往下执行，直到遇到execute()
'''
p = r.pipeline(transaction=True)

r.set('name', 'baobao')
print(r.get('name'))
r.set('name', 'xiaoxiannv')
p.execute()
'''
可以看到并不是
'''
